import matplotlib.pyplot as plt
from random import *
class Monde:
    def __init__(self,dimension):
        self.dimension=dimension
        self.duree=30
        self.carte=[[0 for i in range (self.dimension)]for j in range(self.dimension)]
        for i in range(self.dimension**2//2):#50% des carrés herbus
            a,b=randint(0,self.dimension-1),randint(0,self.dimension-1)
            while self.carte[a][b]!=0: #on vérifie que ce ne soit pas déjà une case herbue
                a,b=randint(0,self.dimension-1),randint(0,self.dimension-1)
            self.carte[a][b]=randint(self.duree,250)
        for i in range(self.dimension): 
            for j in range(self.dimension):
                if self.carte[i][j]==0: #carrés non herbus donc le reste des carrés (ceux qui n'ont pas été modifiés)
                    self.carte[i][j]=randint(0,self.duree-1) 
            
    def herbePousse(self):
        for i in range(self.dimension):
            for j in range(self.dimension):
                self.carte[i][j]+=1
    
    def herbeMangee(self,i,j):
        self.carte[i][j]=0
        
    def nbHerbe(self):
        compteur=0
        for i in self.carte:
            for j in i:
                if j>=self.duree:
                    compteur+=1
        return compteur
    
    def getCoefCarte(self,i,j):
        return self.carte[i][j]







class Etrevivant:
    def __init__(self, position_x, position_y, gain_nourriture):
        
        self.x = position_x
        self.y = position_y
        self.gain = gain_nourriture
        self.energie = randint(gain_nourriture, gain_nourriture * 2)
        self.animal=False #booléen qui passe en True si on trouve un mouton/loup à proximité (pour les classes Loup et chasseur)
        
        self.sexe=choice(["M","F"]) #choisit au hasard si c'est un male ou une femelle


    def deplacement(self, monde, liste_animaux): #pour les classes Loup et chasseur
        self.animal=False
        liste = [(i.x, i.y) for i in liste_animaux]
        if self.x == 0: #première ligne   
            if self.y == 0: #case en haut à droite
                for i in liste:
                    if (i[0] == self.x or i[0] == self.x + 1) and (i[1] == self.y or i[1] == self.y + 1):
                        self.x = i[0]
                        self.y = i[1]
                        self.animal=True
            elif self.y == monde.dimension - 1: #case en haut à gauche
                for i in liste:
                    if (i[0] == self.x or i[0] == self.x +1) and (i[1] == self.y or i[1] == self.y - 1):
                        self.x = i[0]
                        self.y = i[1]
                        self.animal=True
            else : #première ligne
                for i in liste:
                    if (i[0] == self.x or i[0] == self.x +1) and (i[1] == self.y or i[1] == self.y + 1 or i[1]==self.y-1):
                        self.x = i[0]
                        self.y = i[1]
                        self.animal=True
        elif self.x == monde.dimension - 1: #dernière ligne
            if self.y == 0: #case en bas à gauche
                for i in liste:
                    if (i[0] == self.x or i[0] == self.x - 1) and (i[1] == self.y or i[1] == self.y + 1):
                        self.x = i[0]
                        self.y = i[1]
                        self.animal=True
            elif self.y == monde.dimension - 1: #case en bas à droite
                for i in liste:
                    if (i[0] == self.x or i[0] == self.x - 1) and (i[1] == self.y or i[1] == self.y - 1):
                        self.x = i[0]
                        self.y = i[1]
                        self.animal=True
            else : #dernière ligne
                for i in liste:
                    if (i[0] == self.x or i[0] == self.x - 1 ) and (i[1] == self.y or i[1] == self.y - 1 or i[1]==self.y+1):
                        self.x = i[0]
                        self.y = i[1]
                        self.animal=True
        elif self.y == 0: #première colonne (sans la case tout en haut et celle tout en bas)
            for i in liste:
                if (i[0] == self.x or i[0] == self.x+1 or i[0]==self.x-1 ) and (i[1] == self.y or i[1] == self.y + 1):
                    self.x = i[0]
                    self.y = i[1]
                    self.mouton=True
        elif self.y == monde.dimension-1: #dernière colonne (sans la case tout en haut et celle tout en bas)
            for i in liste:
                if (i[0] == self.x or i[0] == self.x-1 or i[0]==self.x+1 ) and (i[1] == self.y or i[1] == self.y -1):
                    self.x = i[0]
                    self.y = i[1]
                    self.animal=True
        else : #cases au milieu
            for i in liste:
                if (i[0] == self.x or i[0] == self.x - 1 or i[0] == self.x + 1) and (i[1] == self.y or i[1] == self.y + 1 or i[1] == self.y - 1):
                    self.x = i[0]
                    self.y = i[1]
                    self.animal=True 
        if not self.animal: #s'il n'y avait pas d'animal à proximité on choisit au hasard
                self.x = choice([self.x - 1, self.x + 1, self.x])  # choisit une des 8 cases adjacentes. il peut ne pas bouger. choice(a) choisit un des éléments dans la liste a
                self.y = choice([self.y - 1, self.y + 1, self.y])
                self.x = (self.x + monde.dimension) % monde.dimension  # formule pour un monde torique.
                self.y = (self.y + monde.dimension) % monde.dimension









class Mouton(Etrevivant): #hérite des méthodes et des attributs de la classe Etrevivant
    
    def variationEnergie(self, monde):
        if monde.getCoefCarte(self.x, self.y) >= monde.duree:  # vérifie si c'est un carré herbu
            self.energie += self.gain
            monde.herbeMangee(self.x,self.y)  # le mouton a mangé l'herbe donc on met le coefficient du carré à 0. de plus, dans le cas où un autre mouton est sur cette case, il n'aura pas le gain d'énergie.
        else:  # ce n'est pas une case herbue
            self.energie -= 1

    def place_mouton(self):
        return Mouton(self.x, self.y,self.gain)
    
    def deplacement(self,monde):
        self.x = choice([self.x - 1, self.x + 1, self.x])  # choisit une des 8 cases adjacentes. il peut ne pas bouger. choice(a) choisit un des éléments dans la liste a
        self.y = choice([self.y - 1, self.y + 1, self.y])
        self.x = (self.x + monde.dimension) % monde.dimension  # formule pour un monde torique.
        self.y = (self.y + monde.dimension) % monde.dimension










class Loup(Etrevivant): 

    def variationEnergie(self, liste_moutons):
        presencem = False
        for i in liste_moutons:
            if i.x == self.x and i.y == self.y:  # s'il y a un mouton sur la même case
                mouton = i
                presencem = True
        if presencem:
            self.energie += self.gain
            liste_moutons.remove(mouton)
        else:
            self.energie -= 1
    

    def place_loup(self):
        return Loup(self.x, self.y,self.gain)








class Chasseur(Etrevivant):
    def __init__(self, position_x,position_y):
        self.x=position_x
        self.y=position_y
        self.choix=0
        
    def tuerLoup(self,liste_loups):
        for i in liste_loups:
            if i.x==self.x and i.y==self.y:#s'il y a un loup sur la même case
                self.choix=randint(0,1)
                if self.choix==0: #simule le fait que le chasseur peut rater le loup
                    liste_loups.remove(i)
                    break #car un chasseur ne peut tuer qu'un loup par tour







class Simulation:
    def __init__(self):
        dimension=int(input("Quelle dimension doit faire la matrice? (50 ou plus) (prop: 50) : "))
        nbmoutons=int(input("Combien de moutons voulez-vous créer?  (prop:400) : "))
        nbloups=int(input("Combien de loups voulez-vous créer? (prop:200) : "))
        nbchasseurs=int(input("Combien de chasseurs voulez-vous créer? (prop:5) : "))
        gainmouton=int(input("Le gain en énergie qu'apporte la nourriture pour le mouton (prop:4) : "))
        gainloup=int(input("Le gain en énergie qu'apporte la nourriture pour le loup (prop:18) : "))
        fin_du_monde=int(input("Nombre de tours maximal de la simulation (prop:400) : "))
        nbfixe=int(input("Nombre de moutons atteint (la simulation continue tant que le nombre de moutons est supérieur à ce nombre) (prop:0) : "))
        tauxreproduction=int(input("Taux de reproduction des moutons (pourcentage) (prop:5) : "))
        tauxreproductionloup=int(input("Taux de reproduction des loups (pourcentage) (prop:6) : "))
        
        self.moutons=[Mouton(randint(0,dimension-1),randint(0,dimension-1),gainmouton) for i in range(nbmoutons)] #leurs coordonnées ne dépassent pas la carte
        self.loups=[Loup(randint(0,dimension-1),randint(0,dimension-1),gainloup) for i in range(nbloups)] 
        self.chasseurs=[Chasseur(randint(0,dimension-1),randint(0,dimension-1)) for i in range(nbchasseurs)]
        self.horloge=0
        self.fin=fin_du_monde
        self.monde=Monde(dimension)
        self.resultats_herbe=[self.monde.nbHerbe()]
        self.resultats_moutons=[len(self.moutons)]
        self.resultats_loups=[len(self.loups)]
        self.nbfixe=nbfixe
        self.taux=round(tauxreproduction*(len(self.moutons)/100)) #nombre de moutons qui se reproduisent par tour
        self.tauxloup=round(tauxreproductionloup*(len(self.loups)/100))
    
    def varier_energie(self,liste):
        if type(liste[0]).__name__ == "Mouton": #on prend le premier élément de la liste pour déterminer si la liste contient des moutons oud es loups
            for i in range(len(liste)): 
                liste[i].variationEnergie(self.monde) #on appelle variationEnergie pour chaque mouton
        else:
            for i in range(len(liste)): 
                liste[i].variationEnergie(self.moutons) #on appelle variationEnergie pour chaque loup
        for animal in liste:
            if animal.energie<=0:
                liste.remove(animal) #supprime les animaux qui n'ont plus d'énergie
                
    def tuerLoup(self):
        for i in range(len(self.chasseurs)):
            self.chasseurs[i].tuerLoup(self.loups) #si un loup se trouve sur la même case que le chasseur il est tué
            
    
    def reproduction(self,liste):
        if type(liste[0]).__name__ == "Mouton":   #on prend le premier élément de la liste pour déterminer si la liste contient des moutons oud es loups
            for i in range(self.taux):
                indice=randint(0,len(liste)-1)
                nouveau_mouton=liste[indice].place_mouton()
                liste.append(nouveau_mouton)
        else: #c'est la liste des loups
            for i in range(self.tauxloup):
                indice=randint(0,len(liste)-1)
                nouveau_loup=liste[indice].place_loup()
                liste.append(nouveau_loup)

    def deplacements(self,liste):
        if type(liste[0]).__name__ == "Loup" : #si c'est la liste des loups 
            for i in range(len(liste)):
                liste[i].deplacement(self.monde,self.moutons)
        elif type(liste[0]).__name__=="Chasseur": #si c'est la liste des chasseurs
            for i in range(len(liste)):
                liste[i].deplacement(self.monde,self.loups)
        else: #si c'est la liste des moutons
            for i in range(len(liste)):
                liste[i].deplacement(self.monde)
            
    def simMouton(self):
        while self.horloge<self.fin and len(self.moutons)>self.nbfixe:
            self.monde.herbePousse()
            self.reproduction(self.moutons)
            if len(self.loups)>0:
                self.reproduction(self.loups) #s'il reste encore des loups
            self.deplacements(self.moutons)
            if len(self.loups)>0: 
                self.deplacements(self.loups)
                self.deplacements(self.chasseurs)
            self.varier_energie(self.moutons)
            if len(self.loups)>0:
                self.varier_energie(self.loups)
                self.tuerLoup() #parcourt la liste des loups et tue s'il y a un chasseur sur la même case
            self.resultats_herbe.append(self.monde.nbHerbe())
            self.resultats_moutons.append(len(self.moutons))
            self.resultats_loups.append(len(self.loups))
            self.horloge+=1
            
        print("Nombre de cases d'herbes: \n",self.resultats_herbe)
        print("Nombre de moutons: \n",self.resultats_moutons)
        print("Nombre de loups: \n",self.resultats_loups)


        x = [i for i in range(self.horloge+1)] #déterminer le nombre de tours
        plt.title("La guerre des moutons")
        plt.xlabel("Nombre de tours")
        plt.ylabel("Quantité")
        plt.plot(x,self.resultats_herbe,label="Herbe")
        plt.plot(x,self.resultats_moutons,label="Moutons")
        plt.plot(x,self.resultats_loups,label="Loups")
        plt.legend()
        plt.show()
        plt.close()



