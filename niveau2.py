import matplotlib.pyplot as plt
from random import *
class Monde:
    def __init__(self):
        self.dimension=dimension
        self.duree=30
        self.carte=[[0 for i in range (self.dimension)]for j in range(self.dimension)]
        for i in range(self.dimension**2//2):#50% des carrés herbus
            a,b=randint(0,self.dimension-1),randint(0,self.dimension-1)
            while self.carte[a][b]!=0: #on vérifie que ce ne soit pas déjà une case herbue
                a,b=randint(0,self.dimension-1),randint(0,self.dimension-1)
            self.carte[a][b]=randint(self.duree,250)
        for i in range(self.dimension): 
            for j in range(self.dimension):
                if self.carte[i][j]==0: #carrés non herbus donc le reste des carrés (ceux qui n'ont pas été modifiés)
                    self.carte[i][j]=randint(0,self.duree-1) 
            
    def herbePousse(self):
        for i in range(self.dimension):
            for j in range(self.dimension):
                self.carte[i][j]+=1
    
    def herbeMangee(self,i,j):
        self.carte[i][j]=0
        
    def nbHerbe(self):
        compteur=0
        for i in self.carte:
            for j in i:
                if j>=self.duree:
                    compteur+=1
        return compteur
    
    def getCoefCarte(self,i,j):
        return self.carte[i][j]

class Etrevivant:
    def __init__(self, position_x, position_y, gain_nourriture):
        
        self.x = position_x
        self.y = position_y
        self.gain = gain_nourriture
        self.energie = randint(gain_nourriture, gain_nourriture * 2)

    def deplacement(self, monde):
        a = [self.x - 1, self.x + 1, self.x]
        b = [self.y - 1, self.y + 1, self.y]
        self.x = choice(a)  # choisit une des 8 cases adjacentes. il peut ne pas bouger. choice(a) choisit un des éléments dans la liste a
        self.y = choice(b)
        self.x = (self.x + monde.dimension) % monde.dimension  # formule pour un monde torique.
        self.y = (self.y + monde.dimension) % monde.dimension

class Mouton(Etrevivant): #hérite des méthodes et des attributs de la classe Etrevivant
    
    def variationEnergie(self, monde):
        if monde.getCoefCarte(self.x, self.y) >= monde.duree:  # vérifie si c'est un carré herbu
            self.energie += self.gain
            monde.herbeMangee(self.x,self.y)  # le mouton a mangé l'herbe donc on met le coefficient du carré à 0. de plus, dans le cas où un autre mouton est sur cette case, il n'aura pas le gain d'énergie.
        else:  # ce n'est pas une case herbue
            self.energie -= 1


    def place_mouton(self):
        return Mouton(self.x, self.y,self.gain)


class Loup(Etrevivant): 

    def variationEnergie(self, liste_moutons):
        presencem = False
        for i in liste_moutons:
            if i.x == self.x and i.y == self.y:  # s'il y a un mouton sur la même case
                mouton = i
                presencem = True
        if presencem:
            self.energie += self.gain
            liste_moutons.remove(mouton)
        else:
            self.energie -= 1

    def place_loup(self):
        return Loup(self.x, self.y,self.gain)


class Simulation:
    def __init__(self):
        dimension=int(input("Quelle dimension doit faire la matrice? (50 ou plus) : "))
        nbmoutons=int(input("Combien de moutons voulez-vous créer? : "))
        nbloups=int(input("Combien de loups voulez-vous créer? : "))
        gainmouton=int(input("Le gain en énergie qu'apporte la nourriture pour le mouton (proposition:4) : "))
        gainloup=int(input("Le gain en énergie qu'apporte la nourriture pour le loup (proposition:5) : "))
        fin_du_monde=int(input("Nombre de tours maximal de la simulation : "))
        nbfixe=int(input("Nombre de moutons atteint (la simulation s'arrêtera lorsque le nombre de moutons aura atteint ce nombre) : "))
        tauxreproduction=int(input("Taux de reproduction des moutons (pourcentage) : "))
        tauxreproductionloup=int(input("Taux de reproduction des loups (pourcentage) : "))
        
        self.moutons=[Mouton(randint(0,dimension-1),randint(0,dimension-1),gainmouton) for i in range(nbmoutons)] #leurs coordonnées ne dépassent pas la carte
        self.loups=[Loup(randint(0,dimension-1),randint(0,dimension-1),gainloup) for i in range(nbloups)] 
        self.horloge=0
        self.fin=fin_du_monde
        self.monde=Monde(dimension)
        self.resultats_herbe=[self.monde.nbHerbe()]
        self.resultats_moutons=[len(self.moutons)]
        self.resultats_loups=[len(self.loups)]
        self.nbfixe=nbfixe
        self.taux=round(tauxreproduction*(len(self.moutons)/100))
        self.tauxloup=round(tauxreproductionloup*(len(self.loups)/100))
    
    def varier_energie(self,liste):
        if type(liste[0]).__name__ == "Mouton": #on prend le premier élément de la liste pour déterminer si la liste contient des moutons oud es loups
            for i in range(len(liste)): 
                liste[i].variationEnergie(self.monde) #on appelle variationEnergie pour chaque mouton
        else:
            for i in range(len(liste)): 
                liste[i].variationEnergie(self.moutons) #on appelle variationEnergie pour chaque loup
        for animal in liste:
            if animal.energie<=0:
                liste.remove(animal) #supprime les animaux qui n'ont plus d'énergie
            
    
    def reproduction(self,liste):
        if type(liste[0]).__name__ == "Mouton":   #on prend le premier élément de la liste pour déterminer si la liste contient des moutons oud es loups
            for i in range(self.taux):
                indice=randint(0,len(liste)-1) #on prend un mouton au hasard
                nouveau_mouton=liste[indice].place_mouton()#création de l'enfant
                liste.append(nouveau_mouton)
        else: #c'est la liste des loups
            for i in range(self.tauxloup):
                indice=randint(0,len(liste)-1) #on prend un loup au hasard
                nouveau_loup=liste[indice].place_loup()#création de l'enfant
                liste.append(nouveau_loup)

    def deplacements(self,liste):
        for i in range(len(liste)):
            liste[i].deplacement(self.monde)

            
    def simMouton(self):
        while self.horloge<self.fin and len(self.moutons)>self.nbfixe:
            self.monde.herbePousse()
            self.reproduction(self.moutons)
            self.reproduction(self.loups)
            self.deplacements(self.moutons)
            self.deplacements(self.loups)
            self.varier_energie(self.moutons)
            self.varier_energie(self.loups)
            self.resultats_herbe.append(self.monde.nbHerbe())
            self.resultats_moutons.append(len(self.moutons))
            self.resultats_loups.append(len(self.loups))
            self.horloge+=1
            
        print("Nombre de cases d'herbes: \n",self.resultats_herbe)
        print("Nombre de moutons: \n",self.resultats_moutons)
        print("Nombre de loups: \n",self.resultats_loups)

        x = [i for i in range(self.horloge+1)] #déterminer le nombre de tours
        plt.title("La guerre des moutons")
        plt.xlabel("Nombre de tours")
        plt.ylabel("Quantité")
        plt.plot(x,self.resultats_herbe,label="Herbe")
        plt.plot(x,self.resultats_moutons,label="Moutons")
        plt.plot(x,self.resultats_loups,label="Loups")
        plt.legend()
        plt.show()
        plt.close()

