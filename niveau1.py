import matplotlib.pyplot as plt
from random import *
class Monde:
    def __init__(self,dimension):
        self.dimension=dimension
        self.duree=30
        self.carte=[[0 for i in range (self.dimension)]for j in range(self.dimension)]
        for i in range(self.dimension**2//2):#50% des carrés herbus
            a,b=randint(0,self.dimension-1),randint(0,self.dimension-1)
            while self.carte[a][b]!=0: #on vérifie que ce ne soit pas déjà une case herbue
                a,b=randint(0,self.dimension-1),randint(0,self.dimension-1)
            self.carte[a][b]=randint(self.duree,250)
        for i in range(self.dimension): 
            for j in range(self.dimension):
                if self.carte[i][j]==0: #carrés non herbus donc le reste des carrés (ceux qui n'ont pas été modifiés)
                    self.carte[i][j]=randint(0,self.duree-1) 
            
    def herbePousse(self):
        for i in range(self.dimension):
            for j in range(self.dimension):
                self.carte[i][j]+=1
    
    def herbeMangee(self,i,j):
        self.carte[i][j]=0
        
    def nbHerbe(self):
        compteur=0
        for i in self.carte:
            for j in i:
                if j>=self.duree:
                    compteur+=1
        return compteur
    
    def getCoefCarte(self,i,j):
        return self.carte[i][j]

class Mouton:
    
    def __init__(self, position_x,position_y,gain_nourriture):
        self.gain=gain_nourriture
        self.x=position_x
        self.y=position_y
        self.energie=randint(gain_nourriture,gain_nourriture*2)
    
    def variationEnergie(self,monde):
        if monde.getCoefCarte(self.x,self.y)>=monde.duree : #vérifie si c'est un carré herbu
            self.energie+=self.gain
            monde.herbeMangee(self.x,self.y) #le mouton a mangé l'herbe donc on met le coefficient du carré à 0. de plus, dans le cas où un autre mouton est sur cette case, il n'aura pas le gain d'énergie. 
        else: #ce n'est pas une case herbue
            self.energie-=1
        
    def deplacement(self,monde):
        a=[self.x-1,self.x+1,self.x]
        b=[self.y-1,self.y+1,self.y]
        self.x=choice(a) #choisit une des 8 cases adjacentes. il peut ne pas bouger. choice(a) choisit un des éléments dans la liste a
        self.y=choice(b)
        self.x=(self.x+monde.dimension)%monde.dimension #formule pour un monde torique. 
        self.y=(self.y+monde.dimension)%monde.dimension
        
    def place_mouton(self):
        return Mouton(self.x,self.y,self.gain)
    
class Simulation:
    def __init__(self):
        dimension=int(input("Quelle dimension doit faire la matrice? (50 ou plus) : "))
        nbmoutons=int(input("Combien de moutons voulez-vous créer? : "))
        fin_du_monde=int(input("Nombre de tours maximal de la simulation : "))
        nourriture=int(input("Le gain en énergie qu'apporte la nourriture pour le mouton (proposition:4) : "))
        nbfixe=int(input("Nombre de moutons atteint (la simulation s'arrêtera lorsque le nombre de moutons aura atteint ce nombre) : "))
        tauxreproduction=int(input("Taux de reproduction des moutons (pourcentage) : "))
        self.moutons=[Mouton(randint(0,dimension-1),randint(0,dimension-1),gainmouton) for i in range(nbmoutons)] #leurs coordonnées ne dépassent pas la carte

         
        self.horloge=0
        self.fin=fin_du_monde
        self.monde=Monde(dimension)
        self.resultats_herbe=[self.monde.nbHerbe()]
        self.resultats_moutons=[len(self.moutons)]
        self.nbfixe=nbfixe
        self.taux=round(tauxreproduction*(len(self.moutons)/100))
    
    def varier_energie(self):
        for i in range(len(self.moutons)): 
            self.moutons[i].variationEnergie(self.monde) #on appelle variationEnergie pour chaque mouton
        for mouton in self.moutons:
            if mouton.energie<=0:
                self.moutons.remove(mouton) #supprime les moutons qui n'ont plus d' énergie 
    
    def reproduction(self):
        for i in range(self.taux):
            indice=randint(0,len(self.moutons)-1) #on prend un mouton au hasard
            nouveau_mouton=self.moutons[indice].place_mouton()#création de l'enfant
            self.moutons.append(nouveau_mouton)
            
    def deplacements(self):
        for i in range(len(self.moutons)):
            self.moutons[i].deplacement(self.monde)
            
    def simMouton(self):
        while self.horloge<self.fin  and len(self.moutons)>self.nbfixe:
            self.monde.herbePousse()
            self.varier_energie()
            self.reproduction()
            self.deplacements()
            self.resultats_herbe.append(self.monde.nbHerbe())
            self.resultats_moutons.append(len(self.moutons))
            self.horloge+=1
            
        print("Nombre de cases d'herbes: \n",self.resultats_herbe)
        print("Nombre de moutons: \n",self.resultats_moutons)

        x = [i for i in range(self.horloge+1)] #liste avec tous les tours
        plt.title("La guerre des moutons")
        plt.xlabel("Nombre de tours")
        plt.ylabel("Quantité")
        plt.plot(x,self.resultats_herbe,label="Herbe")
        plt.plot(x,self.resultats_moutons,label="Moutons")
        plt.legend()
        plt.show()
        plt.close()
